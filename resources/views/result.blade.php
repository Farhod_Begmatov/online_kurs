
@extends('layouts.apps')

@section('content')
    <div class="container">
        <h1>Teachers</h1>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Lesson</th>
                    <th scope="col">Result</th>
                    <th scope="col">Grade</th>
                </tr>
            </thead>
            <tbody>
                @foreach($results as $result)
                    <tr>
                        <td>{{ $result->id }}</td>
                        <td>{{ $result->user->name }}</td>
                        <td>{{ $result->lesson->lesson_name }}</td>
                        <td>{{ $result->result }}</td>
                        <td>{{ $result->grade }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

