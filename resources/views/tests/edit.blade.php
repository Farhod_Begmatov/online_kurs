@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('tests.update', $test->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="subject_id">Subject</label>
                <select class="form-control select2" style="width: 100%;" id="subject_id"
                        name="subject_id">
                    <option>Select subject</option>
                    @foreach($subjects as $subject)
                        <option @if($test->subject_id == $subject->id) selected @endif value="{{ $subject->id }}">{{ $subject->subject_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="lesson_id">Lessons</label>
                <select class="form-control select2" style="width: 100%;" id="lesson_id"
                        name="lesson_id">
                    <option>Select lesson</option>
                    @foreach($lessons as $lesson)
                        <option @if($test->lesson_id == $lesson->id) selected @endif
                            value="{{ $lesson->id }}" >{{ $lesson->lesson_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="question">Question</label>
                <input type="text" class="form-control" id="question"
                       name="question" value="{{ $test->question }}">
            </div>
            <div class="form-group">
                <label for="variant_a">a)</label>
                <input type="radio" name="answer" id="answer" value="1" @if($test->answer == 1) checked @endif>
                <input type="text" class="form-control" id="variant_a"
                       value="{{ $test->variant_a }}" name="variant_a">

                <label for="variant_b">b)</label>
                <input type="radio" name="answer" id="answer" value="2" @if($test->answer == 2) checked @endif>
                <input type="text" class="form-control" id="variant_b"
                       value="{{ $test->variant_b }}" name="variant_b">

                <label for="variant_c">c)</label>
                <input type="radio" name="answer" id="answer" value="3" @if($test->answer == 3) checked @endif>
                <input type="text" class="form-control" id="variant_c"
                       value="{{ $test->variant_c }}" name="variant_c">
            </div>

            <button type="submit" class="btn btn-primary">Edit Test</button>
        </form>
    </div>
@endsection


