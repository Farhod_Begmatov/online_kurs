@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>{{ __('Tests') }}</h1></div>
                    <div class="card-body">
                        <div class="container">
                                <div class="card-header">
                                    {{ $test->id }}. {{ $test->question }}
                                </div>
                                <ul class="list-group list-group-flush" style="margin-bottom: 20px;">
                                    <li class="list-group-item">
                                        <label for="variant_a">a)</label>
                                        <input type="radio" id="answer1" value="1">
                                        {{ $test->variant_a }}
                                    </li>
                                    <li class="list-group-item">
                                        <label for="variant_b">b)</label>
                                        <input type="radio" id="answer2" value="2">
                                        {{ $test->variant_b }}
                                    </li>
                                    <li class="list-group-item"><label for="variant_c">c)</label>
                                        <input type="radio" id="answer3" value="3">
                                        {{ $test->variant_c }}
                                    </li>
                                </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
