@extends('layouts.apps')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Matematika') }}</div>
                <div class="card">

                        @foreach($tests as $test)
                            <div class="card-header">
                                {{ $test->id }}. {{ $test->question }}
                                <input type="hidden" name="data[{{$loop->index}}][test_id]" value="{{ $test->id }}">
                            </div>
                            <ul class="list-group list-group-flush" style="margin-bottom: 20px;">
                                <li class="list-group-item">
                                    <label for="variant_a">a)</label>
                                    <input type="radio" name="data[{{$loop->index}}][answer]" id="answer1" value="1">
                                    {{ $test->variant_a }}
                                </li>
                                <li class="list-group-item">
                                    <label for="variant_b">b)</label>
                                    <input type="radio" name="data[{{$loop->index}}][answer]" id="answer2" value="2">
                                    {{ $test->variant_b }}
                                </li>
                                <li class="list-group-item"><label for="variant_c">c)</label>
                                    <input type="radio" name="data[{{$loop->index}}][answer]" id="answer3" value="3">
                                    {{ $test->variant_c }}
                                </li>

                            </ul>
                        @endforeach
                        <div class="card">
                            <button type="submit" class="btn btn-sm btn-primary">Keyingisi</button>
                        </div>
                    </form>

                </div>

                {{ $tests->links() }}
            </div>
        </div>
    </div>

@endsection
