@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Tests</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Question</th>
                <th scope="col">Variant a</th>
                <th scope="col">Variant b</th>
                <th scope="col">Variant c</th>
                <th scope="col">Answer</th>
                <th scope="col">
                    <a href="{{ route('tests.create') }}">
                        <button class="btn-sm btn-success" type="submit">Add Test</button>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($tests as $test)
                <tr>
                    <td>{{ $test->id }}</td>
                    <td>{{ $test->question }}</td>
                    <td>{{ $test->variant_a }}</td>
                    <td>{{ $test->variant_b }}</td>
                    <td>{{ $test->variant_c }}</td>
                    <td>{{ $test->answer }}</td>
                    <td>
                        <a href="{{ route('tests.edit', $test->id) }}">
                            <button class="btn-sm btn-primary fa fa-pencil" style=" margin-right: 6px;" type="submit">Edit</button>
                        </a>
                        <form action="{{ route('tests.destroy', $test->id) }}" method="POST"  style="display: contents;">
                            @csrf
                            @method('DELETE')
                            <button class="btn-sm btn-danger fa fa-trash" type="submit" onclick="return confirm('Do you want to delete?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $tests->links() }}
    </div>
@endsection

