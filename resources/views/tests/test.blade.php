@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>{{ __('Lessons') }}</h1></div>
                    <div class="card-body">
                        <div class="container">
                            <form method="POST" action="{{ route('results.store') }}" class="form-check">
                                @csrf
                                @method('POST')
                            @foreach($lesson->tests as $test)

                                <div class="card-header">
                                    {{ $loop->index + 1 }}. {{ $test->question }}
                                    <input type="hidden" name="data[{{$loop->index}}][test_id]" value="{{ $test->id }}">
                                    <input type="hidden" name="data[lesson_id]" value="{{ $lesson->id }}">
                                </div>
                                <ul class="list-group list-group-flush" style="margin-bottom: 20px;">
                                    <li class="list-group-item">
                                        <label for="variant_a">a)</label>
                                        <input type="radio" name="data[{{$loop->index}}][answer]" id="answer1" value="1">
                                        {{ $test->variant_a }}
                                    </li>
                                    <li class="list-group-item">
                                        <label for="variant_b">b)</label>
                                        <input type="radio" name="data[{{$loop->index}}][answer]" id="answer2" value="2">
                                        {{ $test->variant_b }}
                                    </li>
                                    <li class="list-group-item"><label for="variant_c">c)</label>
                                        <input type="radio" name="data[{{$loop->index}}][answer]" id="answer3" value="3">
                                        {{ $test->variant_c }}
                                    </li>
                                </ul>
                            @endforeach
                                <div class="card">
                                    <button type="submit" class="btn btn-sm btn-primary">Finish</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
