@extends('layouts.apps')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit User Role</div>
                <div class="card-body">
                    <form action="{{ route('users.update', $user->id) }}">

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
