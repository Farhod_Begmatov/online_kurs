@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('teachers.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" name="name" id="name" value="{{ old('name') }}">
            </div>
            <div class="mb-3">
                <label for="surname" class="form-label">Surname</label>
                <input type="text" class="form-control" name="surname" id="surname" value="{{ old('surname') }}">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control" name="email" id="email" value="{{ old('email') }}">
            </div>
            <div class="form-group">
                <label for="subject_id">Fanlar</label>
                <select class="form-control select2" style="width: 100%;" id="subject_id"
                        name="subject_id">
                    <option>Fanni tanlang</option>
                    @foreach($subjects as $subject)
                        <option @if(old('subject_id') == $subject->id) selected @endif
                            value="{{ $subject->id }}">{{ $subject->subject_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="resume" class="form-label">Resume</label>
                <input type="file" name="resume" id="resume" aria-describedby="inputGroupFileAddon04" aria-label="Upload">
            </div>
            <div class="mb-3">
                <label for="image" class="form-label mr-3">Photo</label>
                <input type="file" name="image" id="image" aria-describedby="inputGroupFileAddon04" aria-label="Upload">
            </div>
            <button type="submit" class="btn btn-primary">Add Teacher</button>
        </form>
    </div>
@endsection


