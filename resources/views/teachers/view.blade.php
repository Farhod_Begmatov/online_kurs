@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>{{ __('Teachers') }}</h1></div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($teachers as $teacher)
                                    <div class="col">
                                        <img src="../storage/{{ $teacher->image }}" class="img-fluid" alt="..." style="height:300px; width:500px">
                                        <h3 class="mt-4">
                                            <p class="card-text">Name: {{ $teacher->name }}</p>
                                            <p class="card-text">Surname: {{ $teacher->surname }}</p>
                                            <p class="card-text">Email: {{ $teacher->email }}</p>
                                            <p class="card-text">Subject: <a href="subjects/{{ $teacher->subject_id }}">{{ $teacher->subject->subject_name }}</a></p>
                                        </h3>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
