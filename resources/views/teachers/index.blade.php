@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Teachers</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Surname</th>
                <th scope="col">Email</th>
                <th scope="col">Subject</th>
                <th scope="col">
                    <a href="{{ route('teachers.create') }}">
                        <button class="btn-sm btn-success" type="submit">Add Teacher</button>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($teachers as $teacher)
                <tr>
                    <td>{{ $teacher->id }}</td>
                    <td>{{ $teacher->name }}</td>
                    <td>{{ $teacher->surname }}</td>
                    <td>{{ $teacher->email }}</td>
                    <td>{{ $teacher->subject->subject_name }}</td>
                    <td>
                        <a href="{{ route('teachers.edit', $teacher->id) }}">
                            <button class="btn-sm btn-primary fa fa-pencil" style=" margin-right: 6px;" type="submit">Edit</button>
                        </a>
                        <form action="{{ route('teachers.destroy', $teacher->id) }}" method="POST"  style="display: contents;">
                            @csrf
                            @method('DELETE')
                            <button class="btn-sm btn-danger fa fa-trash" type="submit" onclick="return confirm('Do you want to delete {{ $teacher->name }}?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $teachers->links() }}
    </div>
@endsection

