@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Lesson</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Subject Name</th>
                <th scope="col">Lesson Name</th>
                <th scope="col">Duration</th>
                <th scope="col">Status</th>
                <th scope="col">
                    <a href="{{ route('lessons.create') }}">
                        <button class="btn-sm btn-success" type="submit">Add Lesson</button>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($lessons as $lesson)
                <tr>
                    <td>{{ $lesson->id }}</td>
                    <td>{{ $lesson->subject->subject_name }}</td>
                    <td>{{ $lesson->lesson_name }}</td>
                    <td>{{ $lesson->duration }}</td>
                    <td>{{ $lesson->status }}</td>
                    <td>
                        <a href="{{ route('lessons.edit', $lesson->id) }}">
                            <button class="btn-sm btn-primary fa fa-pencil" style=" margin-right: 6px;" type="submit">Edit</button>
                        </a>
                        <form action="{{ route('lessons.destroy', $lesson->id) }}" method="POST"  style="display: contents;">
                            @csrf
                            @method('DELETE')
                            <button class="btn-sm btn-danger fa fa-trash" type="submit" onclick="return confirm('Do you want to delete {{ $lesson->lesson_name }}?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $lessons->links() }}
    </div>
@endsection
