@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>{{ __('Lessons') }}</h1></div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($lesson->tests as $test)
                                    <div class="col mb-3">
                                        <video width="400" height="300" @if (auth()->user()->status != 0 || $lesson->status == 0) controls @endif>
                                            <source src="../storage/{{ $lesson->video }}">
                                        </video>
                                        <h6 class="mt-4">
                                            <p class="card-text">Lesson Name: {{ $lesson->lesson_name }} </p>
                                            <p class="card-text d-block">Subject Name: {{ $lesson->subject->subject_name }}</p>
                                            <p class="card-text">Duration: {{ $lesson->duration }}</p>
                                            <p class="card-text">Status: @if ($lesson->status == 0) Free @else Paid @endif</p>
                                            <p class="card-text">Tests: <a href="/lessons/test/{{$lesson->id}}">{{ $lesson->lesson_name }} </a></p>
                                        </h6>
                                        <form action="{{ route('comments.store') }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="mb-3">
                                                <input type="hidden" name="lessonId" id="lessonId" value="{{ $lesson->id }}">
                                                <label for="body">Comments</label> {{ count($lesson->comments) }}
                                                @foreach($lesson->comments as $comment)
                                                    <p>&nbsp;&nbsp;{{ $comment->comment }}</p>
                                                @endforeach
                                                <textarea class="form-control" name="body" id="body" style=" min-width: 500px;"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Comment</button>
                                        </form>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
