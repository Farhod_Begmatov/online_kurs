@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('lessons.update', $lesson->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="subject_id">Fanlar</label>
                <select class="form-control select2" style="width: 100%;" id="subject_id" name="subject_id">
                    <option>Fanni tanlang</option>
                    @foreach($subjects as $subject)
                        <option @if($lesson->subject_id == $subject->id) selected @endif
                        value="{{ $subject->id }}">{{ $subject->subject_name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="lesson_name" class="form-label">Name</label>
                <input type="text" class="form-control" name="lesson_name" id="lesson_name" value="{{ $lesson->lesson_name }}">
            </div>
            <div class="mb-3">
                <label for="video" class="form-label mr-3">Video</label><br>
                <input type="file" name="video" id="video" value="{{ $lesson->video }}">
            </div>
            <div class="mb-3">
                <label for="duration" class="form-label">Duration</label>
                <input type="text" class="form-control" name="duration" id="duration" value="{{ $lesson->duration }}">
            </div>
            <div class="form-check mb-3">
                <input class="form-check-input" type="radio" name="status" id="status" value="0" @if($lesson->status == 0) checked @endif>
                <label class="form-check-label" for="status">
                    Free
                </label>
                <input class="form-check-input ml-4" type="radio" name="status" id="status" value="1" @if($lesson->status == 1) checked @endif>
                <label class="form-check-label ml-5" for="status">
                    Paid
                </label>
            </div>
            <button type="submit" class="btn btn-primary">Edit Lesson</button>
        </form>
    </div>
@endsection


