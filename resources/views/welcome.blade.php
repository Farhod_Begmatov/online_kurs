@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card p-3">
                    <h1>{{ __('Teachers') }}</h1>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($teachers as $teacher)
                                    <div class="col">
                                        <img src="../storage/{{ $teacher->image }}" class="img-fluid" alt="..." style="max-height:300px; max-width:400px">
                                        <h3 class="mt-4">
                                            <p class="card-text">Name: {{ $teacher->name }}</p>
                                            <p class="card-text">Surname: {{ $teacher->surname }}</p>
                                            <p class="card-text">Email: {{ $teacher->email }}</p>
                                            <p class="card-text">Subject: {{ $teacher->subject->subject_name }}</p>
                                        </h3>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="m-auto">
                        {{ $teachers->links() }}
                    </div>
                    <h1>{{ __('Subjects') }}</h1>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($subjects as $subject)
                                    <div class="col">
                                        <h3 class="mt-4">
                                            <p class="card-text">Name: {{ $subject->subject_name }}</p>
                                            <p class="card-text">Surname: {{ $subject->price }}$</p>
                                        </h3>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="m-auto">
                        {{ $subjects->links() }}
                    </div>
                    <h1>{{ __('Lessons') }}</h1>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($lessons as $lesson)
                                    <div class="col">
                                        <h3 class="mt-4">
                                            @if (auth()->check() || $lesson->status != 0)
                                                <a href="{{ url('/login') }}">
                                                    <video width="400" height="300">
                                                        <source src="../storage/{{ $lesson->video }}">
                                                    </video>
                                                </a>
                                            @else
                                                <video width="400" height="300" controls>
                                                    <source src="../storage/{{ $lesson->video }}">
                                                </video>
                                            @endif
                                            <p class="card-text">Lesson Name: {{ $lesson->lesson_name }}</p>
                                            <p class="card-text">Subject Name: {{ $lesson->subject->subject_name }}</p>
                                            <p class="card-text">Duration: {{ $lesson->duration }}</p>
                                            <p class="card-text">Status: @if ($lesson->status == 0) Free @else Paid @endif</p>
                                        </h3>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="m-auto">
                        {{ $lessons->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
