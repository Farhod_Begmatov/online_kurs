@extends('layouts.app')

@section('content')
    <div class="container">
        <form action="{{ route('subjects.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('POST')
            <div class="mb-3">
                <label for="subject_name" class="form-label">Name</label>
                <input type="text" class="form-control" name="subject_name" id="subject_name">
            </div>
            <div class="mb-3">
                <label for="price" class="form-label">Price</label>
                <input type="text" class="form-control" name="price" id="price">
            </div>
            <button type="submit" class="btn btn-primary">Add Subject</button>
        </form>
    </div>
@endsection

