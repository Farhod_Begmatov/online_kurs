@extends('layouts.apps')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h1>{{ __('Subjects') }}</h1></div>
                    <div class="card-body">
                        <div class="container">
                            <div class="row">
                                @foreach($subjects as $subject)
                                    <div class="col">
                                        <h3 class="mt-4">
                                            <p class="card-text">Name: <a href="{{ $subject->id }}">{{ $subject->subject_name }}</a></p>
                                            <p class="card-text">Price: {{ $subject->price }}$</p>
                                        </h3>
                                        <form action="{{ route('comments.store') }}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="mb-3">
                                                <input type="hidden" name="subjectId" id="subjectId" value="{{ $subject->id }}">
                                                <label for="body">Comments</label> {{ count($subject->comments) }}
                                                @foreach($subject->comments as $comment)
                                                    <p>&nbsp;&nbsp;{{ $comment->comment }}</p>
                                                @endforeach
                                                <textarea class="form-control" name="body" id="body" style=" min-width: 500px;"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-primary">Comment</button>
                                        </form>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
