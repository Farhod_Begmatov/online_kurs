@extends('layouts.app')

@section('content')
    <div class="container">

        <h1>Subjects</h1>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Subject Name</th>
                <th scope="col">Price</th>
                <th scope="col">
                    <a href="{{ route('subjects.create') }}">
                        <button class="btn-sm btn-success" type="submit">Create Subject</button>
                    </a>
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($subjects as $subject)
                <tr>
                    <td>{{ $subject->id }}</td>
                    <td>{{ $subject->subject_name }}</td>
                    <td>{{ $subject->price }}</td>
                    <td>
                        <a href="{{ route('subjects.edit', $subject->id) }}">
                            <button class="btn-sm btn-primary fa fa-pencil" style=" margin-right: 6px;" type="submit">Edit</button>
                        </a>
                        <form action="{{ route('subjects.destroy', $subject->id) }}" method="POST"  style="display: contents;">
                            @csrf
                            @method('DELETE')
                            <button class="btn-sm btn-danger fa fa-trash" type="submit" onclick="return confirm('Do you want to delete {{ $subject->name }}?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $subjects->links() }}
    </div>
@endsection
