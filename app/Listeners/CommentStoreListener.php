<?php

namespace App\Listeners;

use App\Events\CommentStoreEvent;
use App\Lesson;
use App\Subject;
use App\Test;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class CommentStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CommentStoreEvent $event)
    {
        $data = $event->getData();
        if (isset($data['lessonId'])){
            $lesson = Lesson::find($data['lessonId']);
            $lesson->comments()->create([
                'comment' => $data['body'],
            ]);
        } elseif (isset($data['subjectId'])){
            $subject = Subject::find($data['subjectId']);
            $subject->comments()->create([
                'comment' => $data['body'],
            ]);
        }elseif(isset($data['testId'])){
            $test = Test::find($data['testId']);
            $test->comments()->create([
                'comment' => $data['body'],
            ]);
        }
    }
}
