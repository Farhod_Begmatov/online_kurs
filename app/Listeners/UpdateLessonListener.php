<?php

namespace App\Listeners;

use App\Events\UpdateLessonEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class UpdateLessonListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateLessonEvent $event)
    {
        $data = $event->getData();
        $lesson = $event->getLesson();
        $video = $lesson->video;
        if ($data['video']){
            $lesson->update([
                'subject_id' => $data['subject_id'],
                'lesson_name' => $data['lesson_name'],
                'video' => $data['video']->store('lessons/video', 'public'),
                'duration' => $data['duration'],
                'status' => $data['status']
            ]);
        }

        if (isset($video)) {
            if (Storage::disk('public')->exists($video)){
                Storage::disk('public')->delete($video);
            }
        }
    }
}
