<?php

namespace App\Listeners;

use App\Events\UpdateTeacherEvent;
use Illuminate\Support\Facades\Storage;

class UpdateTeacherListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UpdateTeacherEvent $event)
    {
        $data = $event->getData();
        $teacher = $event->getTeacher();
        if ($data['image'] || $data['resume']){
            $image = $teacher->image;
            $file = $teacher->file;
            $teacher->update([
                'image' => $data['image']->store('teachers/photo', 'public'),
                'resume' => $data['resume']->store('teachers/resume', 'public'),
                'name' => $data['name'],
                'surname' => $data['surname'],
                'email' => $data['email'],
                'subject_id' => $data['subject_id']
            ]);
        }

        if (isset($file) || isset($image)) {
            if (Storage::disk('public')->exists($file) || Storage::disk('public')->exists($image)){
                Storage::disk('public')->delete($file);
                Storage::disk('public')->delete($image);
            }
        }
    }
}
