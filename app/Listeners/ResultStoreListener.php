<?php

namespace App\Listeners;

use App\Events\ResultStoreEvent;
use App\Result;
use App\Test;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class ResultStoreListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ResultStoreEvent $event)
    {
        $counter = 0;
        $grade = 0;
        $data = $event->getData();
        foreach($data as $value){
            if (!empty($value['test_id'])) {
                $test = Test::where('id', $value['test_id'])->where('answer', $value['answer'])->first();
                if ($test){
                    $counter++;
                    $grade += 10;
                }
            }
        }
        Result::create([
            'user_id' => Auth::user()->id,
            'lesson_id' => $data['lesson_id'],
            'result' => $counter,
            'grade' => $grade
        ]);
    }
}
