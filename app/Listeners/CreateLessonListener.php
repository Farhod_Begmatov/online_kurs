<?php

namespace App\Listeners;

use App\Events\CreateLessonEvent;
use App\Lesson;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\InteractsWithQueue;

class CreateLessonListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreateLessonEvent $event)
    {
        $data = $event->getData();
        if ($data['video'])
            $data['video'] = $data['video']->store('lessons/video', 'public');
        Lesson::create($data);
    }
}
