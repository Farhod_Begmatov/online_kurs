<?php

namespace App\Listeners;

use App\Events\CreateTeacherEvent;
use App\Teacher;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\InteractsWithQueue;

class CreateTeacherListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CreateTeacherEvent $event)
    {
        $data = $event->getData();
        if ($data['image'] || $data['resume']){
            $data['image'] = $data['image']->store('teachers/photo', 'public');
            $data['resume'] = $data['resume']->store('teachers/resume', 'public');
        }
        $teacher = new Teacher();
        $teacher->create($data);
    }
}
