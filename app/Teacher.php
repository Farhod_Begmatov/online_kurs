<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = ['subject_id', 'name', 'surname', 'email', 'resume', 'image'];

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
