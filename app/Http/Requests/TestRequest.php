<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_id' => 'required|integer',
            'lesson_id' => 'required|integer',
            'question' => 'required|string',
            'answer' => 'required|integer',
            'variant_a' => 'required|string',
            'variant_b' => 'required|string',
            'variant_c' => 'required|string',
        ];
    }
}
