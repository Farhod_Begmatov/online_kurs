<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject_id' => 'required|integer',
            'lesson_name' => 'required|string',
            'video' => 'required|mimes:mp4,mov,ogg | max:20000',
            'duration' => 'required|numeric',
            'status' => 'required|integer',
        ];
    }
}
