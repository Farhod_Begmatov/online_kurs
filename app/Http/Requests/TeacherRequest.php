<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'surname' => 'required|string',
            'email' => 'required|email|unique:teachers',
            'resume' => 'required|mimes:csv,txt,pdf|max:8192',
            'image' => 'required|mimes:jpeg, png, bmp, gif, svg',
            'subject_id' => 'required|integer'
        ];
    }
}
