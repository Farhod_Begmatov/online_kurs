<?php

namespace App\Http\Controllers;

use App\Events\CreateLessonEvent;
use App\Events\UpdateLessonEvent;
use App\Http\Requests\LessonRequest;
use App\Lesson;
use App\Subject;
use Illuminate\Http\Request;

class LessonController extends Controller
{
    public function index()
    {
        $lessons = Lesson::with('subject')->paginate();
        return view('lessons.index', compact('lessons'));
    }

    public function create()
    {
        $subjects = Subject::all();
        return view('lessons.create', compact('subjects'));
    }

    public function store(LessonRequest $request)
    {
        $commentEvent = new CreateLessonEvent();
        $commentEvent->setData($request->validated());
        event($commentEvent);

        return redirect()->route('lessons.index');

    }

    public function show(Lesson $lesson)
    {
        return view('lessons.show', ['lesson' => $lesson]);
    }

    public function edit(Lesson $lesson)
    {
        $subjects = Subject::all();
        return view('lessons.edit', ['lesson' => $lesson, 'subjects' => $subjects]);
    }

    public function update(Request $request, Lesson $lesson)
    {
        $updateLessonEvent = new UpdateLessonEvent();
        $updateLessonEvent->setData($request->all());
        $updateLessonEvent->setLesson($lesson);
        event($updateLessonEvent);
        return redirect()->route('lessons.index');
    }

    public function destroy($id)
    {
        //
    }
}
