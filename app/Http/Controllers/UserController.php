<?php

namespace App\Http\Controllers;

use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $users = User::all();
        return view('admin.users.index', ['users' => $users]);
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('users,edit')->with([
            'user' => $user,
            'roles' => $roles
        ]);
    }


    public function update(Request $request, User $user)
    {

    }

    public function destroy($id)
    {
        //
    }
}
