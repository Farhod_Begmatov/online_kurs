<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Subject;
use App\Teacher;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function welcome()
    {
        $teachers = Teacher::with('subject')->paginate(6);
        $subjects = Subject::with('teachers', 'lessons')->paginate(6);
        $lessons = Lesson::with('subject')->paginate(6);
        return view('welcome', compact(['teachers', 'subjects', 'lessons']));
    }
}
