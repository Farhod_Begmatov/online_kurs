<?php

namespace App\Http\Controllers;

use App\Lesson;
use App\Subject;
use App\Teacher;
use App\Test;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $teachers = Teacher::with('subject')->paginate(6);
        $subjects = Subject::with('teachers', 'lessons')->paginate(6);
        $lessons = Lesson::with('subject')->paginate(6);
        return view('home', compact(['teachers', 'subjects', 'lessons']));
    }

    public function teachers()
    {
        $teachers = Teacher::with('subject')->paginate();
        return view('teachers.view', compact('teachers'));
    }

    public function subjects()
    {
        $subjects = Subject::with('teachers', 'lessons', 'comments')->paginate();
        return view('subjects.view', compact('subjects'));
    }

    public function lessons()
    {
        $lessons = Lesson::with('subject')->paginate();
        return view('lessons.view', compact('lessons'));
    }

    public function tests()
    {
        $tests = Test::paginate();

        return view('tests.view', compact('tests'));
    }
}
