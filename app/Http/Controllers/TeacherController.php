<?php

namespace App\Http\Controllers;

use App\Events\CreateTeacherEvent;
use App\Events\UpdateTeacherEvent;
use App\Http\Requests\TeacherRequest;
use App\Subject;
use App\Teacher;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class TeacherController extends Controller
{
    public function index()
    {
        $teachers = Teacher::paginate();
        return view('teachers.index', compact('teachers'));
    }

    public function create()
    {
        $subjects = Subject::all();
        return view('teachers.create', compact('subjects'));
    }

    public function store(TeacherRequest $request)
    {
        $createTeacherEvent = new CreateTeacherEvent();
        $createTeacherEvent->setData($request->validated());
        event($createTeacherEvent);

        return redirect()->route('teachers.index');
    }

    public function show(Teacher $teacher)
    {
        $subjects = Subject::all();
        return view('teachers.view', ['subjects' => $subjects, 'teacher' => $teacher]);
    }

    public function edit(Teacher $teacher)
    {
        $subjects = Subject::all();
        return view('teachers.edit', ['teacher' => $teacher, 'subjects' => $subjects]);
    }

    public function update(Request $request, Teacher $teacher)
    {
        $updateTeacherEvent = new UpdateTeacherEvent();
        $updateTeacherEvent->setData($request->all());
        $updateTeacherEvent->setTeacher($teacher);
        event($updateTeacherEvent);
        return redirect()->route('teachers.index');
    }

    public function destroy(Teacher $teacher)
    {
        $teacher->delete();
        return redirect()->route('teachers.index');
    }
}
