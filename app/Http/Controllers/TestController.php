<?php

namespace App\Http\Controllers;

use App\Events\CreateTestEvent;
use App\Http\Requests\TestRequest;
use App\Lesson;
use App\Subject;
use App\Test;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index()
    {
        $tests = Test::paginate();
        return view('tests.index', ['tests' => $tests]);
    }

    public function create()
    {
        $subjects = Subject::all();
        $lessons = Lesson::all();
        return view('tests.create', ['subjects' => $subjects, 'lessons' => $lessons]);
    }

    public function store(TestRequest $request)
    {
        Test::create($request->validated());
        return redirect()->route('tests.index');
    }

    public function show(Test $test)
    {
        return view('tests.show', ['test' => $test]);
    }

    public function edit(Test $test)
    {
        $subjects = Subject::all();
        $lessons = Lesson::all();
        return view('tests.edit', ['test' => $test, 'subjects' => $subjects, 'lessons' => $lessons]);
    }

    public function update(Request $request, Test $test)
    {
        $test->update($request->all());
        return redirect()->route('tests.index');
    }

    public function destroy(Test $test)
    {
        $test->delete();
        return redirect()->route('tests.index');
    }

    public function test($id)
    {
        $subject_id = Subject::where('id', $id)->value('id');
        $lesson = Lesson::with('tests')->where('subject_id', $subject_id)->first();
        return view('tests.test', compact('lesson'));
    }
}
