<?php

namespace App\Http\Controllers;

use App\Events\CommentStoreEvent;
use App\Lesson;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(Request $request)
    {
        $commentevent = new CommentStoreEvent();
        $commentevent->setData($request->all());
        event($commentevent);
        return redirect()->back();
    }

    public function destroy($id)
    {

    }
}
