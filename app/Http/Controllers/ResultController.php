<?php

namespace App\Http\Controllers;

use App\Events\ResultStoreEvent;
use App\Result;
use App\Test;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResultController extends Controller
{
    public function index()
    {
        $results = Result::query()->paginate(10);
        return view('result', compact('results'));
    }

    public function store(Request $request)
    {
        $data = $request->data;
        $resultStoreEvent = new ResultStoreEvent();
        $resultStoreEvent->setData($data);
        event($resultStoreEvent);

        return redirect()->route('results.index');

    }

}
