<?php

namespace App\Providers;

use App\Events\CommentStoreEvent;
use App\Events\CreateLessonEvent;
use App\Events\CreateSubjectEvent;
use App\Events\CreateTeacherEvent;
use App\Events\ResultStoreEvent;
use App\Events\UpdateLessonEvent;
use App\Events\UpdateTeacherEvent;
use App\Listeners\CommentStoreListener;
use App\Listeners\CreateLessonListener;
use App\Listeners\CreateSubjectListener;
use App\Listeners\CreateTeacherListener;
use App\Listeners\ResultStoreListener;
use App\Listeners\UpdateLessonListener;
use App\Listeners\UpdateTeacherListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        CommentStoreEvent::class => [
            CommentStoreListener::class,
        ],

        CreateTeacherEvent::class => [
            CreateTeacherListener::class,
        ],

        UpdateTeacherEvent::class => [
            UpdateTeacherListener::class,
        ],

        CreateLessonEvent::class => [
            CreateLessonListener::class,
        ],

        UpdateLessonEvent::class => [
            UpdateLessonListener::class,
        ],

        ResultStoreEvent::class => [
            ResultStoreListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
