<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', 'admin')->first();
        $teacherRole = Role::where('name', 'teacher')->first();
        $studentRole = Role::where('name', 'student')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Farhod',
            'email' => 'farhodktiat@gmail.com',
            'password' => Hash::make('password')
        ]);

        $teacher = User::create([
            'name' => 'Laziz',
            'email' => 'lazizazizov@gmail.com',
            'password' => Hash::make('password')
        ]);

        $student = User::create([
            'name' => 'Abbos',
            'email' => 'abdullayevabbos@gmail.com',
            'password' => Hash::make('password')
        ]);

        $user = User::create([
            'name' => 'User',
            'email' => 'user@user.com',
            'password' => Hash::make('password')
        ]);

        $admin->roles()->attach($adminRole);
        $teacher->roles()->attach($teacherRole);
        $student->roles()->attach($studentRole);
        $user->roles()->attach($userRole);
    }
}
