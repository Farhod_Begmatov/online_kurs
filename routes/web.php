<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', 'Controller@welcome')->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/teachers/list', 'HomeController@teachers')->name('teachers');
Route::get('/subjects/list', 'HomeController@subjects')->name('subjects');
Route::get('/lessons/list', 'HomeController@lessons')->name('lessons');
Route::get('/tests/list', 'HomeController@tests')->name('tests');
Route::get('/lessons/test/{id}', 'TestController@test')->name('test');

Route::resource('teachers', 'TeacherController');
Route::resource('subjects', 'SubjectController');
Route::resource('lessons', 'LessonController');
Route::resource('tests', 'TestController');
Route::resource('results', 'ResultController');
Route::resource('comments', 'CommentController');
Route::resource('admin/users', 'UserController', ['except' => ['create', 'store', 'show']]);

